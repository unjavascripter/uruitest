'use strict';

const mongodb = require('mongodb')
const Db = mongodb.Db;
const Server = mongodb.Server;
const ObjectID = mongodb.ObjectID;
const express = require('express');
const bodyParser= require('body-parser')
const app = express();

app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));


// CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods',' GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

// DB stuff

let db = new Db('uruiTest', new Server('localhost', 27017));
// Open DB connection
db.open((err, db) => {

  // Create a collection
  db.createCollection('matches', (err, result) => {
    if(!err){
        db.collection('matches').deleteMany( {}, (err, results) => {
          console.log('Mongo Collection created')  
        });
    }
  });

});


// Game Logic

let gameRules = [
  {
    option: "rock",
    owns: ["scissors"],
    pwnedBy: ["paper"]
  },
  {
    option: "paper",
    owns: ["rock"],
    pwnedBy: ["scissors"]
  },
  {
    option: "scissors",
    owns: ["paper"],
    pwnedBy: ["rock"]
  },
]

function getWinner(home, visitor) {
  if(home === visitor){
    return 'tie';
  }

  let homeRule = gameRules.filter(rule => {
    return home === rule.option;
  })
    
  return homeRule[0].owns.indexOf(visitor) !== -1 ? 'home' : homeRule[0].pwnedBy.indexOf(visitor) !== -1 ? 'visitor' : 'err'; 
  
  
}


// API

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/api/options', (req, res) => {
  return res.send(gameRules.map(e => e.option)).end();
});

app.get('/api/rules', (req, res) => {
  return res.send(gameRules).end();
});

app.put('/api/rules', (req, res) => {
  gameRules = req.body;
  return res.send(gameRules).end();
});

app.post('/api/match', (req, res) => {
  let matchObj = req.body;

  matchObj.gameWinner = getWinner(matchObj.home.option, matchObj.visitor.option);
    
  db.collection('matches').findOne({},{},{"matchId": matchObj.matchId}, (err, result) => {

    if(!result) {
      matchObj.gameScores = {home: 0, visitor: 0, tie: 0};
      matchObj.gameWinnerHistory = [];
    }else{

      matchObj.gameScores = result.gameScores;
      matchObj.gameWinnerHistory = result.gameWinnerHistory;
      result.matchWinner ? matchObj.matchWinner = result.matchWinner : false;

    }

    if(matchObj.matchWinner) {
      return res.status(200).send(matchObj).end();
    }

    matchObj.gameWinnerHistory.push(matchObj.gameWinner);
    matchObj.gameScores[matchObj.gameWinner] += 1;

    if(matchObj.gameScores.home === 3 || matchObj.gameScores.visitor === 3) {
        matchObj.matchWinner = matchObj.gameScores.home === 3 ? 'home' : 'visitor';
    }

    db.collection('matches').update({"matchId": matchObj.matchId}, matchObj, {upsert: true}, (err, result) => {
      return res.status(200).send(matchObj).end();
    })
  

  });


});




app.listen(8002, _ => console.log('API running on port 8002'));