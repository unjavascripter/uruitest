(function(){
  'use strict';

  function config($urlRouterProvider) {
     $urlRouterProvider.otherwise('/');    
  }

  angular
    .module('uruiTest')
      .config(config);

})();