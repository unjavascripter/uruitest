'use strict';

(function(){
   function ctrlFn ($stateParams) {
     this.home = $stateParams.home;
     this.visitor = $stateParams.visitor;
   }

  function config($stateProvider) {
    $stateProvider.state(
      {
        name: 'game',
        url: '/game/:home/:visitor',
        template: '<game home="$ctrl.home" visitor="$ctrl.visitor"></game>',
        controller: ctrlFn,
        controllerAs: '$ctrl'
      }
    )
  }

  angular
    .module('uruiTest')
      .config(config);

})();