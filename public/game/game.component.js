(function(){
  'use strict';

  function ctrlFn(rulesFactory, gameFactory) {
    let matchId = `${this.home}vs${this.visitor}${Date.now()}`;
    rulesFactory.getOptions().then(response => {
      this.gameOptions = response.data;
    });

    this.checkWinner = function() {
      gameFactory.checkWinner({
        matchId: matchId,
        home: {
          name:this.home,
          option: this.home_selectedOption
        },
        visitor: {
          name:this.visitor,
          option: this.visitor_selectedOption
        },
      }).then(response => {
        this.results = response.data;
        this.home_ready = false;
      });
    }
  }

  angular
    .module('uruiTest')
      .component('game', {
        bindings: {
          home: '<',
          visitor: '<'
        },
        controller: ctrlFn,
        templateUrl: './game/game.html'
       });
})();