(function(){
  'use strict';

  function ctrlFn($state) {
    this.setPlayers = function() {
      $state.go('game', {home: this.home, visitor: this.visitor});
    }
  }

  angular
    .module('uruiTest')
      .component('mainThing', {
        controller: ctrlFn,
        templateUrl: './main/main.html'
       });
})();