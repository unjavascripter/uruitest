'use strict';

(function(){
  function config($stateProvider) {
    $stateProvider.state(
      {
        name: 'main',
        url: '/',
        template: '<main-thing></main-thing>'
      }
    )
  }

  angular
    .module('uruiTest')
      .config(config);

})();