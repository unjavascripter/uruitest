'use strict';

(function(){
  function rulesFactory($http) {
    let _rulesFactory_ = {};

    _rulesFactory_.getOptions = function() {
      return $http.get('/api/options');
    }
    
    _rulesFactory_.get = function() {
      return $http.get('/api/rules');
    }
    
    _rulesFactory_.set = function(rulesArr) {
      return $http.put('/api/rules', rulesArr);
    }

    return _rulesFactory_;
  }

  angular
    .module('uruiTest')
      .factory('rulesFactory', rulesFactory);

})();