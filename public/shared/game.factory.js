'use strict';

(function(){
  function gameFactory($http) {
    let _gameFactory_ = {};

    _gameFactory_.checkWinner = function(matchObj) {
      return $http.post('/api/match', matchObj);
    }

    return _gameFactory_;
  }

  angular
    .module('uruiTest')
      .factory('gameFactory', gameFactory);

})();