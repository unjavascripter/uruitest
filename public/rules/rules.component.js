(function(){
  'use strict';

  function ctrlFn(rulesFactory) {

    let getRules = _ => {
      rulesFactory.get().then(response => {
        this.gameRules = response.data;
        this.newRules = JSON.stringify(this.gameRules);
      });
    }

    getRules();

    this.changeTheRules = function() {
      rulesFactory.set(JSON.parse(this.newRules));
      getRules();
      
    }
  }

  angular
    .module('uruiTest')
      .component('rules', {
        controller: ctrlFn,
        templateUrl: './rules/rules.html'
       });
})();