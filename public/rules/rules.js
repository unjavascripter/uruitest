'use strict';

(function(){
  function config($stateProvider) {
    $stateProvider.state(
      {
        name: 'rules',
        url: '/rules',
        template: '<rules></rules>'
      }
    )
  }

  angular
    .module('uruiTest')
      .config(config);

})();